package ru.t1.aksenova.tm.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.component.Server;

import java.net.Socket;

public abstract class AbstractServerSocketTask extends AbstractServerTask {

    @NotNull
    protected final Socket socket;

    public AbstractServerSocketTask(
            @NotNull final Socket socket,
            @NotNull final Server server
    ) {
        super(server);
        this.socket = socket;
    }

}
