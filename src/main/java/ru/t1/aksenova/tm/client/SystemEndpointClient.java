package ru.t1.aksenova.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.api.endpoint.ISystemEndpoint;
import ru.t1.aksenova.tm.dto.request.ServerAboutRequest;
import ru.t1.aksenova.tm.dto.request.ServerVersionRequest;
import ru.t1.aksenova.tm.dto.response.ServerAboutResponse;
import ru.t1.aksenova.tm.dto.response.ServerVersionResponse;

public final class SystemEndpointClient extends AbstractEndpoint implements ISystemEndpoint {

    @NotNull
    @Override
    @SneakyThrows
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request) {
        return (ServerAboutResponse) call(request);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerVersionResponse getVersion(@NotNull ServerVersionRequest request) {
        return (ServerVersionResponse) call(request);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final SystemEndpointClient client = new SystemEndpointClient();
        client.connect();
        @NotNull final ServerAboutResponse serverAboutResponse = client.getAbout(new ServerAboutRequest());
        System.out.println("[About]");
        System.out.println("AUTHOR: " + serverAboutResponse.getName());
        System.out.println("E-MAIL: " + serverAboutResponse.getEmail());

        @NotNull final ServerVersionResponse serverVersionResponse = client.getVersion(new ServerVersionRequest());
        System.out.println("[Version]");
        System.out.println(serverVersionResponse.getVersion());

        client.disconnect();
    }

}
